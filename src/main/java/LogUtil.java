import com.google.common.flogger.FluentLogger;

public class LogUtil {

    static {
//        System.setProperty("flogger.backend_factory","com.agsimeonov.flogger.backend.fluentd.FluentdBackendFactory#getInstance");
//        System.setProperty("flogger.backend_factory","custom.fluency.logger.FluencyBackendFactory#getInstance");
        System.setProperty("flogger.backend_factory","com.flogger.fluency.backend.FluencyBackendFactory#getInstance");
    }
    private static final FluentLogger logger = FluentLogger.forEnclosingClass();

    public void logData(String data){
        logger.atInfo().log(data);
        logger.atInfo().log("hello fluency");
    }
}
